from odoo import models, fields, api

class Tingkat(models.Model):
    _name = 'openacademy.tingkat'
    # _rec_name = 'tingkat'

    @api.multi
    def name_get(self):
        res = []
        for rec in self:
            # name = 'tk: ' + rec.tingkat <#error
            # name = 'tk: ' + str(rec.tingkat)
            # name = 'tk: {} adalah {}  adalah {}'.format(rec.tingkat)
            # name = 'tk: {tingkat} adalah {tingkat}'.format(tingkat=rec.tingkat)

            name = 'tk: {}'.format(rec.tingkat)
            res.append((rec.id, name))
        return res

    tingkat = fields.Integer()

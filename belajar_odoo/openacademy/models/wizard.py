# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Wizard(models.TransientModel):
    _name = 'openacademy.wizard'
    _description = "Wizard: Quick Registration of Attendees to courses"

    def _default_courses(self):
        return self.env['openacademy.course'].browse(self._context.get('active_ids'))

    course_ids = fields.Many2many('openacademy.course',
        string="courses", required=True, default=_default_courses)
    attendee_ids = fields.Many2many('res.partner', string="Attendees")

    @api.multi
    def subscribe(self):
        for course in self.course_ids:
            course.attendee_ids |= self.attendee_ids
        return {}

class Wizard2(models.TransientModel):
    _name = 'openacademy.wizard2'
    _description = "Wizard: Quick Registration of Attendees to courses"

    info = fields.Text()
    student_ids = fields.Many2many('res.partner', string="Student", domain=[('is_student','=',True)])

    @api.multi
    def modifikasi_data(self):
        for student in self.student_ids:
            student.comment = self.info
        return {}
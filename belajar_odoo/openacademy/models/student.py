# -*- coding: utf-8 -*-
from odoo import fields, models

class Student(models.Model):
    _inherit = 'res.partner'

    is_student = fields.Boolean('Student', default=False)
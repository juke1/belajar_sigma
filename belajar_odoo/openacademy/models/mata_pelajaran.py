from odoo import models, fields, api

class MataPelajaran(models.Model):
    _name = 'openacademy.mata.pelajaran'

    name = fields.Char(string="Title", required=True)
    description = fields.Text()
    tipe_mata_pelajaran = fields.Selection([('ipa', 'IPA'), ('ips', 'IPS')])


    ikon_filename = fields.Char()
    ikon = fields.Binary()

# -*- coding: utf-8 -*-
from odoo import fields, models

class Instructor(models.Model):
    _inherit = 'res.partner'

    is_instructor = fields.Boolean("Instructor", default=False)

    session_ids = fields.Many2many('openacademy.session',
        string="Attended Sessions", readonly=False)
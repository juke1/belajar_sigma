# -*- coding: utf-8 -*-
from odoo import fields, models, api

from datetime import datetime
from odoo.exceptions import ValidationError, AccessError

class Session(models.Model):
    _name = 'openacademy.session'
    _description = "OpenAcademy Sessions"

    #function yang di exec waktu simpan Add new / create new record
    #return record / self
    #vals = isinya adalah semua field yang di create
    @api.model
    def create(self, vals):


        #
        name = vals.get('name')
        # name = "EDIT : {} ".format(name)
        # vals['name'] = name
        #

        model_mata_pelajaran = self.env['openacademy.mata.pelajaran']
        vals_untuk_mata_pelajaran = {
            'name': 'Nama Otomatis dari: {}'.format(name),
            'tipe_mata_pelajaran': 'ips'
        }
        record_baru_mata_pelajaran = model_mata_pelajaran.create(vals_untuk_mata_pelajaran)


        vals['name'] = "EDIT : {} | barusan nambah mata pelajaran: {}".format(vals['name'], record_baru_mata_pelajaran.id)
        vals['mata_pelajaran_id'] = record_baru_mata_pelajaran.id

        return super(Session, self).create(vals)

    #function yang di exec waktu simpan edit record
    #recutn boolean
    #    #vals = isinya adalah semua field yang di edit

    @api.multi
    def write(self, vals):
        name = vals.get('name')

        mata_pelajaran_id = self.mata_pelajaran_id
        mata_pelajaran_id.write({
            'name' : 'HASIL EDIT DARI {}'.format(name)
        })

        return super(Session, self).write(vals)

    def default_name(self):
        from random import randint
        x = randint(0, 9)

        model_mata_pelajaran = self.env['openacademy.mata.pelajaran']
        last_rec = model_mata_pelajaran.search([('tipe_mata_pelajaran', '=', 'ipa')], limit=1)
        nama_mata_pelajaran_ipa = ''
        if last_rec:
            nama_mata_pelajaran_ipa = last_rec.name
        return 'NAMA BARU {} | {}'.format(x, nama_mata_pelajaran_ipa)

    name = fields.Char(required=True, default=default_name)


    @api.onchange('start_date')
    def onchange_start_date(self):
        if self.start_date:
            # start_date = datetime.strptime(self.start_date, '%Y-%m-%d')
            start_date = self.start_date
            if start_date < datetime.today().date():
                raise ValidationError('Can not go back date')

    start_date = fields.Date(default=fields.Date.today)
    mata_pelajaran_id = fields.Many2one('openacademy.mata.pelajaran')

    active = fields.Boolean(default=True)

    duration = fields.Float(digits=(6, 2), help="Duration in days")
    seats = fields.Integer(string="Number of seats")

    course_id = fields.Many2one('openacademy.course',
                                ondelete='cascade', string="Course", required=True)


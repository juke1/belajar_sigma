from odoo import models, fields, api
from odoo.exceptions import ValidationError, AccessError

class Course(models.Model):
    _name = 'openacademy.course'

    name = fields.Char(string="Title", required=True, size=10)
    description = fields.Text()

    tingkat_id = fields.Many2one('openacademy.tingkat')

    mata_pelajaran_id = fields.Many2one('openacademy.mata.pelajaran')
    mata_pelajaran_name = fields.Char('openacademy.mata.pelajaran', related='mata_pelajaran_id.name')
    tipe_mata_pelajaran = fields.Selection([('ipa', 'IPA'), ('ips', 'IPS')],
       related="mata_pelajaran_id.tipe_mata_pelajaran", store=True,
        )

    #related 1 record

    # mata_pelajaran_ids = fields.Many2many()
    # #related ke banyak record
    #
    # mata_pelajaran_ids = fields.One2many()
    # #related ke children -- Header - Detail

    seats = fields.Integer(string="Number of seats")

    course_id = fields.Many2one('openacademy.course',
                                ondelete='cascade', string="Course", required=True)

    instructor_id = fields.Many2one('res.partner',
                                ondelete='cascade', string="Instructor", required=True, domain=[('is_instructor','=',True)])

    attendee_ids = fields.Many2many('res.partner', string="Attendees", domain=[('is_student','=',True)])

    taken_seats = fields.Float(string="Taken seats", compute='_taken_seats')

    @api.depends('seats', 'attendee_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0.0
            else:
                r.taken_seats = 100.0 * len(r.attendee_ids) / r.seats

    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.instructor_id and r.instructor_id in r.attendee_ids:
                raise ValidationError("A session's instructor can't be an attendee")